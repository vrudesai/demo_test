"""Netskope ITSM plugin."""

import re
from typing import List
from datetime import datetime, timedelta
import requests

from netskope.integrations.itsm.plugin_base import PluginBase, ValidationResult
from netskope.integrations.itsm.models import Alert


RAW_KEYS = [
    "srcip",
    "dstip",
    "site",
    "nsdeviceuid",
    "activity",
    "ccl",
    "cci",
    "managementID",
    "url",
    "event_type",
    "shared_credential_user",
    "userkey",
    "breach_media_reference",
    "email_source",
    "breach_score",
    "breach_date",
    "matched_username",
    "hostname",
    "device",
    "os",
    "browser",
    "access_method",
    "device_classification",
    "mime_type",
    "policy",
    "md5",
    "sha256",
    "object",
    "filesize",
    "instance_id",
    "dlp_incident_id",
    "dlp_file",
    "act_user",
    "dlp_rule",
    "dlp_rule_count",
    "dlp_profile",
    "lh_custodian_email",
    "lh_custodian_name",
    "lh_dest_app",
    "lh_shared",
    "instance",
    "file_path",
    "lh_dest_instance",
    "lh_original_filename",
    "legal_hold_profile_name",
    "modified",
    "referer",
    "action",
    "malsite_id",
    "malicious",
    "app_session_id",
    "malsite_category",
    "page",
    "threat_match_field",
    "local_md5",
    "local_sha256",
    "dtection_type",
    "malware_id",
    "ns_detection_name",
    "detection_engine",
    "malware_scanner_result",
    "malware_type",
    "malware_profile",
    "q_original_filename",
    "q_app",
    "quarantine_profile",
    "transaction_id",
    "quarantine_file_name",
    "q_admin",
    "q_instance",
    "file_type",
    "sa_profile_name",
    "sa_rule_name",
    "account_id",
    "iaas_asset_tags",
    "sa_rule_remediation",
    "asset_object_id",
]


class NetskopePlugin(PluginBase):
    """Netskope plugin implementation."""

    def _validate_auth(self, configuration) -> ValidationResult:
        """Validate authentication step."""
        alert_endpoint = f"https://{configuration['auth']['tenant_name'].strip()}.goskope.com/api/v1/alerts"
        start_time = datetime.now()
        params = {
            "token": configuration["auth"]["token"],
            "starttime": start_time.timestamp(),
            "endtime": start_time.timestamp(),
        }
        try:
            response = requests.get(
                alert_endpoint,
                params=params,
                headers={
                    "User-Agent": f"cto-{configuration['auth']['tenant_name']}"
                },
            )
            response.raise_for_status()
            if response.json().get("status") == "success":
                return ValidationResult(
                    success=True, message="Validation successfull."
                )
            else:
                return ValidationResult(
                    success=False,
                    message=f"Error occurred while connecting to Netskope. {''.join(response.json().get('errors'))}",
                )
        except Exception as ex:
            self.logger.error(
                "Netskope ITSM: Error occurred while validating configuration."
            )
            self.logger.error(
                re.sub(r"token=(.*?)&", "token=********&", str(repr(ex)))
            )
        return ValidationResult(
            success=False,
            message="Could not validate authentication credentials.",
        )

    def _validate_params(self, configuration):
        try:
            if not (0 <= int(configuration["params"]["days"]) <= 365):
                return ValidationResult(
                    success=False,
                    message="Initial range must be in the range 0-365.",
                )
            if not configuration["params"]["type"]:
                return ValidationResult(
                    success=False,
                    message="Atleast one alert type must be selected.",
                )
            return ValidationResult(
                success=True, message="Validation successful."
            )
        except ValueError:
            return ValidationResult(
                success=False, message="Invalid number of days provided."
            )

    def validate_step(self, name, configuration):
        """Validate a given step."""
        if name == "auth":
            return self._validate_auth(configuration)
        elif name == "params":
            return self._validate_params(configuration)
        else:
            return ValidationResult(
                success=True, message="Validation successful."
            )

    def _get_raw_dict(self, alert):
        return {
            k: str(v)
            for k, v in {k: alert.get(k) for k in RAW_KEYS}.items()
            if v is not None
        }

    def pull_alerts(self) -> List[Alert]:
        """Pull alerts from the Netskope platform."""
        # Let's trip the spaces from the tenant name.
        self.configuration["auth"]["tenant_name"] = self.configuration["auth"][
            "tenant_name"
        ].strip()
        config = self.configuration
        if self.last_run_at is None:
            start_time = datetime.now() - timedelta(
                days=int(self.configuration["params"]["days"])
            )
        else:
            start_time = self.last_run_at
        end_time = datetime.now()
        alert_endpoint = f"https://{config['auth']['tenant_name']}.goskope.com/api/v1/alerts"
        alerts = []
        for alert_type in config["params"]["type"]:
            params = {
                "token": config["auth"]["token"],
                "starttime": start_time.timestamp(),
                "endtime": end_time.timestamp(),
                "type": alert_type,
            }
            if config["params"]["query"].strip():
                params["query"] = config["params"]["query"].strip()
            try:
                response = requests.get(
                    alert_endpoint,
                    params=params,
                    headers={
                        "User-Agent": f"cto-{config['auth']['tenant_name']}"
                    },
                )
                response.raise_for_status()
                response_json = response.json()
                if response_json.get("status") == "error":
                    raise requests.HTTPError(
                        f"Error occurred while connecting to Netskope. {''.join(response_json.get('errors'))}"
                    )
                raw_alerts = response_json.get("data", [])
                for raw_alert in raw_alerts:
                    raw_fields = self._get_raw_dict(raw_alert)
                    alerts.append(
                        Alert(
                            id=raw_alert.get("_id"),
                            alertName=raw_alert.get("alert_name"),
                            alertType=raw_alert.get("alert_type"),
                            app=raw_alert.get("app"),
                            appCategory=raw_alert.get("appcategory"),
                            type=raw_alert.get("type"),
                            user=raw_alert.get("user"),
                            timestamp=datetime.fromtimestamp(
                                raw_alert.get("timestamp")
                            ),
                            rawAlert=raw_fields,
                        )
                    )
            except Exception as ex:
                self.logger.error(
                    re.sub(r"token=(.*?)&", "token=********&", str(repr(ex)))
                )
                raise requests.HTTPError(
                    "Netskope ITSM: Error occurred while pulling alerts."
                )
        return alerts
