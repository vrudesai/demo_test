"""CrowdStrike Plugin implementation to push and pull the data from CrowdStrike Platform."""

import requests
import datetime
import re
from typing import List
from netskope.integrations.cte.plugin_base import (
    PluginBase,
    ValidationResult,
    PushResult,
)
from netskope.integrations.cte.models import Indicator, IndicatorType

CROWDSTRIKE_TO_INTERNAL_TYPE = {
    "hash_md5": IndicatorType.MD5,
    "hash_sha256": IndicatorType.SHA256,
    "domain": IndicatorType.URL,
    "sha256": IndicatorType.SHA256,
    "md5": IndicatorType.MD5,
}

INTERNAL_TYPES_TO_CROWDSTRIKE = {
    IndicatorType.MD5: "md5",
    IndicatorType.SHA256: "sha256",
    IndicatorType.URL: "domain",
}


class CrowdStrikePlugin(PluginBase):
    """CrowdStrikePlugin class having concrete implementation for pulling and pushing threat information."""

    def handle_error(self, resp):
        """Handle the different HTTP response code.

        Args:
            resp (requests.models.Response): Response object returned from API call.
        Returns:
            dict: Returns the dictionary of response JSON when the response code is 200.
        Raises:
            HTTPError: When the response code is not 200.
        """
        if resp.status_code == 200 or resp.status_code == 201:
            try:
                return resp.json()
            except ValueError:
                self.notifier.error(
                    "Plugin: CrowdStrike,"
                    "Exception occurred while parsing JSON response."
                )
                self.logger.error(
                    "Plugin: CrowdStrike, "
                    "Exception occurred while parsing JSON response."
                )
        elif resp.status_code == 401:
            self.notifier.error(
                "Plugin: CrowdStrike, "
                "Received exit code 401, Authentication Error"
            )
            self.logger.error(
                "Plugin: CrowdStrike, "
                "Received exit code 401, Authentication Error"
            )
        elif resp.status_code == 403:
            self.notifier.error(
                "Plugin: CrowdStrike, "
                "Received exit code 403, Forbidden User"
            )
            self.logger.error(
                "Plugin: CrowdStrike, "
                "Received exit code 403, Forbidden User"
            )
        elif resp.status_code >= 400 and resp.status_code < 500:
            self.notifier.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP client Error"
            )
            self.logger.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP client Error"
            )
        elif resp.status_code >= 500 and resp.status_code < 600:
            self.notifier.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP server Error"
            )
            self.logger.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP server Error"
            )
        else:
            self.notifier.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP Error"
            )
            self.logger.error(
                f"Plugin: CrowdStrike, "
                f"Received exit code {resp.status_code}, HTTP Error"
            )
        resp.raise_for_status()

    def get_indicators_detailed(self, ioc_ids, headers):
        """Get detailed information by Detection IDs.

        Args:
            ioc_ids (dict): Python dict object having Indicators IDs received from Query endpoint.
            headers (dict): Header dict having Auth token as bearer header.
        Returns:
            List[cte.models.Indicators]: List of indicator objects received from the CrowdStrike platform.
        """
        # Indicator endpoint, this will return detailed information about Indicator.
        indicator_endpoint = f"{self.configuration['base_url']}/detects/entities/summaries/GET/v1"
        indicator_list = []
        json_payload = {}
        for ioc_chunks in self.divide_in_chunks(ioc_ids, 1000):
            json_payload["ids"] = list(ioc_chunks)
            headers = self.reload_auth_token(headers)
            ioc_resp = requests.post(
                indicator_endpoint,
                headers=headers,
                json=json_payload,
                verify=self.ssl_validation,
                proxies=self.proxy,
            )
            resp_json = self.handle_error(ioc_resp)
            if resp_json.get("errors"):
                err_msg = resp_json.get("errors")[0].get("message")
                self.notifier.error(
                    f"Plugin: CrowdStrike Unable to Fetch Indicator Details, "
                    f"Error: {err_msg}"
                )
                self.logger.error(
                    f"Plugin: CrowdStrike Unable to Fetch Indicator Details, "
                    f"Error: {err_msg}"
                )
            indicators_json_list = resp_json.get("resources", [])
            for indicator_json in indicators_json_list:
                behaviors = indicator_json.get("behaviors", [])
                if behaviors:
                    for behavior_info in behaviors:
                        if (
                            len(behavior_info.get("ioc_value")) > 0
                            and len(behavior_info.get("ioc_type")) > 0
                        ):
                            indicator_list.append(
                                Indicator(
                                    value=behavior_info.get("ioc_value"),
                                    type=CROWDSTRIKE_TO_INTERNAL_TYPE.get(
                                        behavior_info.get("ioc_type")
                                    ),
                                    comments=behavior_info.get(
                                        "ioc_description", ""
                                    ),
                                    firstSeen=datetime.datetime.strptime(
                                        behavior_info.get("timestamp"),
                                        "%Y-%m-%dT%H:%M:%SZ",
                                    ),
                                    lastSeen=datetime.datetime.strptime(
                                        behavior_info.get("timestamp"),
                                        "%Y-%m-%dT%H:%M:%SZ",
                                    ),
                                )
                            )
                        else:
                            self.logger.warn(
                                "Plugin: CrowdStrike: Skipping the record as IOC value and/or IOC type not found."
                            )
        return indicator_list

    def get_ioc_ids(self, threat_type, headers):
        """Get the all the IOC ID list from the Indicator Query Endpoint.

        Args:
            threat_type (string): Type of threat data to pull.
            headers (dict): Header dict object having OAUTH2 access token.
        Returns:
            dict: JSON response dict received from query Indicator endpoint.
        """
        # Query endpoint, this will return all the indicator IDs.
        query_endpoint = (
            f"{self.configuration['base_url']}/indicators/queries/iocs/v1"
        )
        query_params = {}
        if threat_type == "Both":
            query_params["types"] = ["md5", "sha256", "domain"]
        elif threat_type == "Malware":
            query_params["types"] = ["md5", "sha256"]
        elif threat_type == "URL":
            query_params["types"] = "domain"
        query_params["limit"] = 500
        offset = 0
        ioc_ids = []
        while True:
            query_params["offset"] = offset
            headers = self.reload_auth_token(headers)
            all_ioc_resp = requests.get(
                query_endpoint,
                headers=headers,
                params=query_params,
                verify=self.ssl_validation,
                proxies=self.proxy,
            )
            ioc_resp_json = self.handle_error(all_ioc_resp)
            errors = ioc_resp_json.get("errors")
            if errors:
                err_msg = errors[0].get("message", "")
                self.notifier.error(
                    "Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
                self.logger.error(
                    "Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
                raise requests.HTTPError(
                    f"Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
            meta = ioc_resp_json.get("meta")
            offset = meta.get("pagination", {}).get("offset")
            total = meta.get("pagination", {}).get("total")
            ioc_ids.extend(ioc_resp_json.get("resources", []))
            if offset == total:
                break
        return ioc_ids

    def get_detection_ids(self, threat_type, headers):
        """Get the all the Detection ID list from the Detection Endpoint.

        Args:
            threat_type (string): Type of threat data to pull.
            headers (dict): Header dict object having OAUTH2 access token.
        Returns:
            dict: JSON response dict received from Detection endpoint.
        """
        # Query endpoint, this will return all the indicator IDs.
        query_endpoint = (
            f"{self.configuration['base_url']}/detects/queries/detects/v1"
        )
        if self.last_run_at:
            last_run_time = self.last_run_at
        else:
            last_run_time = datetime.datetime.now() - datetime.timedelta(
                days=self.configuration["days"]
            )
        last_run_time = last_run_time.strftime("%Y-%m-%dT%H:%M:%SZ")
        query_params = {}
        ioc_types = []
        if threat_type == "Both":
            ioc_types = ["hash_md5", "hash_sha256", "domain"]
        elif threat_type == "Malware":
            ioc_types = [
                "hash_md5",
                "hash_sha256",
            ]
        elif threat_type == "URL":
            ioc_types = ["domain"]
        query_params["limit"] = 9999
        ioc_ids = []
        for ioc_type in ioc_types:
            filter_query = (
                f"last_behavior:>'{last_run_time}'"
                f"+behaviors.ioc_type:'{ioc_type}'"
            )
            query_params["filter"] = filter_query
            headers = self.reload_auth_token(headers)
            all_ioc_resp = requests.get(
                query_endpoint,
                headers=headers,
                params=query_params,
                verify=self.ssl_validation,
                proxies=self.proxy,
            )
            ioc_resp_json = self.handle_error(all_ioc_resp)
            errors = ioc_resp_json.get("errors")
            if errors:
                err_msg = errors[0].get("message", "")
                self.notifier.error(
                    "Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
                self.logger.error(
                    "Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
                raise requests.HTTPError(
                    f"Plugin: CrowdStrike Unable to Fetch Indicators, "
                    f"Error: {err_msg}"
                )
            ioc_ids.extend(ioc_resp_json.get("resources", []))
        return ioc_ids

    def pull(self):
        """Pull the Threat information from CrowdStrike platform.

        Returns:
            List[cte.models.Indicators]: List of indicator objects received from the CrowdStrike platform.
        """
        # Let's trip the spaces from the OAUTH2 secrets.
        self.configuration["client_id"] = self.configuration[
            "client_id"
        ].replace(" ", "")
        self.configuration["client_secret"] = self.configuration[
            "client_secret"
        ].replace(" ", "")
        config = self.configuration
        if config["is_pull_required"] == "Yes":
            self.logger.info("Plugin: CrowdStrike Polling is enabled.")
            threat_type = config["threat_data_type"]
            try:
                auth_json = self.get_auth_json(
                    self.configuration.get("client_id"),
                    self.configuration.get("client_secret"),
                    self.configuration.get("base_url"),
                )
                auth_token = auth_json.get("access_token")
                headers = {"Authorization": f"Bearer {auth_token}"}
                ioc_ids = self.get_detection_ids(threat_type, headers)
                return self.get_indicators_detailed(ioc_ids, headers)

            except requests.exceptions.ProxyError:
                self.notifier.error(
                    "Plugin: CrowdStrike Invalid proxy configuration."
                )
                self.logger.error(
                    "Plugin: CrowdStrike Invalid proxy configuration."
                )
                raise requests.HTTPError(
                    "Plugin: CrowdStrike Invalid proxy configuration."
                )
            except requests.exceptions.ConnectionError:
                self.notifier.error(
                    "Plugin: CrowdStrike Unable to establish connection with CrowdStrike platform. "
                    "Proxy server or CrowdStrike API is not reachable."
                )
                self.logger.error(
                    "Plugin: CrowdStrike Unable to establish connection with CrowdStrike platform. "
                    "Proxy server or CrowdStrike API is not reachable."
                )
                raise requests.HTTPError(
                    "Plugin: CrowdStrike Unable to establish connection with CrowdStrike platform. "
                    "Proxy server or CrowdStrike API is not reachable."
                )
            except requests.exceptions.RequestException as e:
                self.logger.error(
                    "Plugin: CrowdStrike "
                    "Exception occurred while making an API call to CrowdStrike platform"
                )
                raise e
        else:
            self.logger.info(
                "Plugin: CrowdStrike Polling is disabled, skipping."
            )
            return []

    def push(self, indicators: List[Indicator]):
        """Push the Indicator list to CrowdStrike.

        Args:
            indicators (List[cte.models.Indicators]): List of Indicator objects to be pushed.
        Returns:
            cte.plugin_base.PushResult: PushResult object with success flag and Push result message.
        """
        self.logger.info("Plugin: CrowdStrike Executing push method")
        # Let's trip the spaces from the OAUTH2 secrets.
        self.configuration["client_id"] = self.configuration[
            "client_id"
        ].replace(" ", "")
        self.configuration["client_secret"] = self.configuration[
            "client_secret"
        ].replace(" ", "")
        try:
            auth_json = self.get_auth_json(
                self.configuration.get("client_id"),
                self.configuration.get("client_secret"),
                self.configuration.get("base_url"),
            )
            auth_token = auth_json.get("access_token")
            headers = {"Authorization": f"Bearer {auth_token}"}
            ioc_ids = self.get_ioc_ids("Both", headers)
            payload_list = self.prepare_payload(ioc_ids, indicators)
            for chunked_list in self.divide_in_chunks(
                payload_list, self.configuration["batch_size"]
            ):
                headers = self.reload_auth_token(headers)
                self.push_indicators_to_crowdstrike(headers, chunked_list)
            self.logger.info(
                "Plugin: CrowdStrike "
                f"Successfully Pushed {len(payload_list)} Indicators to CrowdStrike"
            )
            return PushResult(
                success=True,
                message=f"Successfully Pushed {len(payload_list)} Indicators to CrowdStrike",
            )
        except requests.exceptions.ProxyError:
            self.notifier.error(
                "Plugin: CrowdStrike Invalid proxy configuration."
            )
            self.logger.error(
                "Plugin: CrowdStrike Invalid proxy configuration."
            )
            return PushResult(
                success=False,
                message=(
                    "Failed to push indicators to CrowdStrike "
                    "Invalid proxy configuration"
                ),
            )
        except requests.exceptions.ConnectionError:
            self.notifier.error(
                "Plugin: CrowdStrike Unable to establish connection with CrowdStrike platform. "
                "Proxy server or CrowdStrike API is not reachable."
            )
            self.logger.error(
                "Plugin: CrowdStrike Unable to establish connection with CrowdStrike platform. "
                "Proxy server or CrowdStrike API is not reachable."
            )
            return PushResult(
                success=False,
                message=(
                    "Failed to push indicators to CrowdStrike "
                    "Unable to establish connection with CrowdStrike platform."
                ),
            )
        except requests.exceptions.RequestException as e:
            self.logger.error(
                "Plugin: CrowdStrike "
                "Exception occurred while making an API call to CrowdStrike platform"
            )
            return PushResult(
                success=False,
                message=(
                    "Exception occurred while making an API call to CrowdStrike platform "
                    f"Error :{repr(e)}"
                ),
            )

    def reload_auth_token(self, headers):
        """Reload the OAUTH2 token after Expiry."""
        if self.storage["token_expiry"] < datetime.datetime.now():
            self.logger.info(
                "Plugin: Crowdstrike OAUTH2 token expired generating new token"
            )
            auth_json = self.get_auth_json(
                self.configuration.get("client_id"),
                self.configuration.get("client_secret"),
                self.configuration.get("base_url"),
            )
            auth_token = auth_json.get("access_token")
            headers = {"Authorization": f"Bearer {auth_token}"}
            return headers
        else:
            return headers

    def divide_in_chunks(self, indicators, chunk_size):
        """Return Fixed size chunks from list."""
        for i in range(0, len(indicators), chunk_size):
            yield indicators[i : i + chunk_size]  # noqa

    def push_indicators_to_crowdstrike(self, headers, json_payload):
        """Push the indicator to the CrowdStrike endpoint.

        Args:
            headers (dict): Header dict object having OAUTH2 access token
            json_payload (List[dict]): List of python dict object of JSON response model as per CrowdStrike API.)
        Returns:
            dict: JSON response dict received after successfull Push.
        """
        push_endpoint = (
            f"{self.configuration['base_url']}/indicators/entities/iocs/v1"
        )
        try:
            post_resp = requests.post(
                push_endpoint,
                headers=headers,
                json=json_payload,
                verify=self.ssl_validation,
                proxies=self.proxy,
            )
        except requests.exceptions.RequestException as e:
            self.notifier.error(
                "Plugin: CrowdStrike "
                f"Exception occurred while making an API call to CrowdStrike platform {repr(e)}"
            )
            self.logger.error(
                "Plugin: CrowdStrike "
                f"Exception occurred while making an API call to CrowdStrike platform {repr(e)}"
            )
            return {}
        json_resp = self.handle_error(post_resp)
        errors = json_resp.get("errors")

        if errors:
            err_msg = errors[0].get("message", "")
            self.notifier.error(
                "Plugin: CrowdStrike Unable to Push Indicators,"
                f"Error: {err_msg}"
            )
            self.logger.error(
                "Plugin: CrowdStrike Unable to Push Indicators,"
                f"Error: {err_msg}"
            )
            raise requests.HTTPError(
                f"Plugin: CrowdStrike Unable to Push Indicators,"
                f"Error: {err_msg}"
            )
        return json_resp

    def _extract_host(self, url):
        try:
            host_regex = r"^(?:[a-zA-Z]*:\/\/)?([\w\-\.]+)(?:\/)?"
            host = re.findall(host_regex, url).pop()
            if host:
                return host
            else:
                raise ValueError("Could not extract host name")
        except Exception:
            return url

    def prepare_payload(self, ioc_ids, indicators):
        """Prepare the JSON payload for Push.

        Args:
            ioc_ids (List[str]):
            indicators (List[cte.models.Indicators]): List of Indicator objects to be pushed.
        Returns:
            List[dict]: List of python dict object of JSON response model as per CrowdStrike API.
        """
        payload_list = []
        json_body = {
            "source": self.configuration.get("source", ""),
            "policy": self.configuration.get("policy", ""),
            "share_level": self.configuration.get("share_level", ""),
        }
        for indicator in indicators:
            if (
                f"{INTERNAL_TYPES_TO_CROWDSTRIKE[indicator.type]}:{indicator.value}"
                not in ioc_ids
            ):
                json_body["type"] = INTERNAL_TYPES_TO_CROWDSTRIKE[
                    indicator.type
                ]
                if indicator.type == IndicatorType.URL:
                    json_body["value"] = self._extract_host(indicator.value)
                else:
                    json_body["value"] = indicator.value
                payload_list.append(json_body.copy())
        return payload_list

    def validate(self, data):
        """Validate the Plugin configuration parameters.

        Args:
            data (dict): Dict object having all the Plugin configuration parameters.
        Returns:
            cte.plugin_base.ValidateResult: ValidateResult object with success flag and message.
        """
        self.logger.info(
            "Plugin: CrowdStrike Executing validate method for CrowdStrike plugin"
        )
        if "base_url" not in data or data["base_url"] not in [
            "https://api.crowdstrike.com",
            "https://api.us-2.crowdstrike.com",
            "https://api.laggar.gcw.crowdstrike.com",
            "https://api.eu-1.crowdstrike.com",
        ]:
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Type of Pulling configured should be non-empty string."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Enable Polling' provided. Allowed values are 'Yes', or 'No'.",
            )

        if (
            "client_id" not in data
            or not data["client_id"]
            or type(data["client_id"]) != str
        ):
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred"
                "Error: Type of Client ID should be non-empty string."
            )
            return ValidationResult(
                success=False, message="Invalid Client ID provided.",
            )

        if (
            "client_secret" not in data
            or not data["client_secret"]
            or type(data["client_secret"]) != str
        ):
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred"
                "Error: Type of Client Secret should be non-empty string."
            )
            return ValidationResult(
                success=False, message="Invalid Client Secret provided.",
            )

        if "is_pull_required" not in data or data["is_pull_required"] not in [
            "Yes",
            "No",
        ]:
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Type of Pulling configured should be non-empty string."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Enable Polling' provided. Allowed values are 'Yes', or 'No'.",
            )

        if "threat_data_type" not in data or data["threat_data_type"] not in [
            "Both",
            "Malware",
            "URL",
        ]:
            self.logger.error(
                "Plugin: CrowdStrike Invalid value for 'Type of Threat data to pull' provided. "
                "Allowed values are Both, Malware or URL."
            )
            return ValidationResult(
                success=False,
                message=(
                    "Invalid value for 'Type of Threat data to pull' provided. "
                    "Allowed values are 'Both', 'Malware' or 'URL'."
                ),
            )
        if (
            "days" not in data
            or not data["days"]
            or type(data["days"]) != int
            or data["days"] < 0
        ):
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Type of Initial Range (in days) should be non-zero positive integer."
            )
            return ValidationResult(
                success=False,
                message="Error: Type of Initial Range (in days) should be non-zero positive integer.",
            )
        if (
            "batch_size" not in data
            or not data["batch_size"]
            or type(data["batch_size"]) != int
            or data["batch_size"] < 0
        ):
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Type of Indicate Batch Size Range should be non-zero positive integer."
            )
            return ValidationResult(
                success=False,
                message="Error: Type of Indicate Batch Size Range should be non-zero positive integer.",
            )
        if "share_level" in data and type(data["share_level"]) != str:
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Invalid Type of CrowdStrike Share level provided."
            )
            return ValidationResult(
                success=False,
                message="Invalid Type of CrowdStrike Share level provided.",
            )
        if "policy" in data and data["policy"] not in ["detect", "none"]:
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Invalid Value of CrowdStrike Detect Policy provided. "
                "Allowed values are 'detect' and 'none'."
            )
            return ValidationResult(
                success=False,
                message="Invalid Value of CrowdStrike Detect Policy provided.Allowed values are 'detect' and 'none'.",
            )
        if "source" in data and (
            type(data["source"]) != str or len(data["source"]) > 200
        ):
            self.logger.error(
                "Plugin: CrowdStrike Validation error occurred "
                "Error: Invalid Type of IOC source provided."
            )
            return ValidationResult(
                success=False,
                message=(
                    "Invalid Type of IOC source provided. "
                    "Size of source string should be less than 200 Characters"
                ),
            )

        return self.validate_auth_params(
            data["client_id"], data["client_secret"], data["base_url"]
        )

    def validate_auth_params(self, client_id, client_secret, base_url):
        """Validate the authentication params with CrowdStrike platform.

        Args:
            client_id (str): Client ID required to generate OAUTH2 token.
            client_secret (str): Client Secret required to generate OAUTH2 token.
            base_url (str): Base url of crowd strike
        Returns:
            ValidationResult: ValidationResult object having validation results after making
            an API call.
        """
        try:
            self.get_auth_json(client_id, client_secret, base_url)
            return ValidationResult(
                success=True,
                message="Validation successfull for CrowdStrike Plugin",
            )
        except requests.exceptions.ProxyError:
            self.logger.error(
                "Plugin: CrowdStrike Validation Error, "
                "Invalid proxy configuration."
            )
            return ValidationResult(
                success=False,
                message="Validation Error, Invalid proxy configuration.",
            )
        except requests.exceptions.ConnectionError:
            self.logger.error(
                "Plugin: CrowdStrike Validation Error, "
                "Unable to establish connection with CrowdStrike Platform API"
            )
            return ValidationResult(
                success=False,
                message="Validation Error, Unable to establish connection with CrowdStrike Platform API",
            )
        except requests.HTTPError as err:
            self.logger.error(
                f"Plugin: CrowdStrike Validation Error, "
                f"Error in validating Credentials {repr(err)}"
            )
            return ValidationResult(
                success=False,
                message=f"Validation Error, Error in validating Credentials {repr(err)}",
            )

    def get_auth_json(self, client_id, client_secret, base_url):
        """Get the OAUTH2 Json object with access token from CrowdStrike platform.

        Args:
            client_id (str): Client ID required to generate OAUTH2 token.
            client_secret (str): Client Secret required to generate OAUTH2 token.
            base_url (str): Base URL of crowdstrike.
        Returns:
            json: JSON response data in case of Success.
        """
        client_id = client_id.replace(" ", "")
        client_secret = client_secret.replace(" ", "")
        auth_endpoint = f"{base_url}/oauth2/token"

        auth_params = {
            "grant_type": "client_credentials",
            "client_id": client_id,
            "client_secret": client_secret,
        }
        resp = requests.post(
            auth_endpoint,
            data=auth_params,
            verify=self.ssl_validation,
            proxies=self.proxy,
        )
        auth_json = self.handle_error(resp)
        auth_errors = auth_json.get("errors")
        if auth_errors:
            err_msg = auth_errors[0].get("message", "")
            self.notifier.error(
                "Plugin: CrowdStrike Unable to generate Auth token. "
                f"Error: {err_msg}"
            )
            self.logger.error(
                "Plugin: CrowdStrike Unable to generate Auth token. "
                f"Error: {err_msg}"
            )
            raise requests.HTTPError(
                f"Plugin: CrowdStrike Unable to generate Auth token. "
                f"Error: {err_msg}"
            )
        if self.storage is not None:
            self.storage[
                "token_expiry"
            ] = datetime.datetime.now() + datetime.timedelta(
                seconds=int(auth_json.get("expires_in", 1799))
            )
        return auth_json
